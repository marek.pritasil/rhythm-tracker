//  RhythmStroke.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2021 Marek Přitasil. All rights reserved.
//


import Foundation

enum Stroke {
    case UP
    case DOWN
}

enum StrokeState {
    case VALID
    case INVALID
    case PENDING
}

class RhythmStroke {
    private(set) var stroke: Stroke!
    private(set) var state: StrokeState = StrokeState.PENDING
    
    init(_ stroke: Stroke) {
        self.stroke = stroke
    }
    
    func updateState(valid: Bool) {
        self.state = (valid) ? StrokeState.VALID : StrokeState.INVALID
    }
    
    func clear() {
        self.state = StrokeState.PENDING
    }
}
