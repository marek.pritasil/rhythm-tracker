//  Buffer.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2021 Marek Přitasil. All rights reserved.
//


import Foundation

class Buffer {
    private var size: Int = 0
    private var values: [Double] = []
    
    init(size: Int) {
        self.size = size
        self.reset()
    }
    
    func reset() {
        self.values.removeAll()
    }
    
    func add(_ value: Double) {
        self.values.insert(value, at:0)
        if(self.values.count > self.size) {
            self.values.removeLast()
        }
    }
    
    func sum() -> Double {
        return self.values.reduce(0.0, +)
    }
    
    func isFull() -> Bool {
        return self.size == self.values.count
    }
}
    
