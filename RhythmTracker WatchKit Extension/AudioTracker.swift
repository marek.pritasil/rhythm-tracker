//  AudioTracker.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2020 Marek Přitasil. All rights reserved.
//


import Foundation
import AVFoundation
import os.log

protocol AudioTrackerDelegate: class {
    func soundDetected()
}

class AudioTracker {
    private var audioRecorder: AVAudioRecorder?
    private  var recording: Bool = false
    private var timer: Timer?
    private var outputFile: URL?
    private var recentlyNotified: Bool = false
    weak var delegate: AudioTrackerDelegate?
    
    private var prevLevel: Float = Float.infinity
    
    func startRecording(interval: TimeInterval) {
        if(self.recording) {
            os_log("Rhythm: already recording")
            return;
        }
        let fileGuid = "\(UUID().uuidString)s.m4a"
        let audioFilename = getDocumentsDirectory().appendingPathComponent(fileGuid)
        self.outputFile = audioFilename
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record, mode: .measurement)
        } catch _ {
            os_log("Rhythm: Error occurred")
        }
        
        let settings: [String: Int] = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            self.audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            self.audioRecorder?.prepareToRecord()
            self.audioRecorder?.isMeteringEnabled = true
            self.audioRecorder?.record()
            self.recording = true
            self.timer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(updateLevels), userInfo: nil, repeats: true)
        } catch{
            os_log("Rhythm: No recording available")
        }
        
    }
    
    func stopRecording() {
        self.recording = false
        self.timer?.invalidate()
        self.audioRecorder?.stop()
        self.audioRecorder?.deleteRecording()
        do {
            try AVAudioSession.sharedInstance().setActive(false, options: .notifyOthersOnDeactivation)
        } catch _ {
            os_log("Error occurred")
        }
    }
    
    @objc
    func updateLevels(){
        self.audioRecorder?.updateMeters()
        let level = self.audioRecorder?.averagePower(forChannel: 0) ?? -120
        
        if(level > -40 && abs(level - self.prevLevel) > 2 && self.prevLevel != Float.infinity){
            self.delegate?.soundDetected()
        }
        
        self.prevLevel = level
    }
    
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}
