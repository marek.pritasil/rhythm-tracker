//  RhythmSelectionController.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2021 Marek Přitasil. All rights reserved.
//


import Foundation
import WatchKit
import os


class RhythmSelectionController: WKInterfaceController {
    @IBOutlet weak var upBtn: WKInterfaceButton!
    @IBOutlet weak var downBtn: WKInterfaceButton!
    @IBOutlet weak var saveBtn: WKInterfaceButton!
    @IBOutlet weak var lbl: WKInterfaceLabel!
    
    private var rhythm: [Stroke] = []
    private var rhythmTracker: RhythmTracker?
    private let MAX_RHYTM_LENTH: Int = 6

    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context);
        self.rhythmTracker = context as? RhythmTracker
        self.rhythm = self.rhythmTracker!.rhythmStrokes
        self.updateLabel()
    }

    @IBAction func upBtnPress() {
        self.updateRhythm(stroke: Stroke.UP)
    }
    
    @IBAction func downBtnPress() {
        self.updateRhythm(stroke: Stroke.DOWN)
    }
    
    @IBAction func saveBtnPress() {
        self.rhythmTracker!.updateRhythm(self.rhythm)
        pop()
    }
    
    private func updateRhythm(stroke: Stroke) {
        if( self.rhythm.count == self.MAX_RHYTM_LENTH){
            self.rhythm.removeAll()
        }
        
        self.rhythm.append(stroke)
        self.updateLabel()
    }
    
    private func updateLabel() {
        self.lbl.setText(Helper.stringifyRhythm(self.rhythm))
    }
}
