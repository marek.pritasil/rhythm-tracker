//  Rhythm.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2021 Marek Přitasil. All rights reserved.
//


import Foundation

class Rhythm {
    private var rhythm: [RhythmStroke] = []
    private var currentPosition = 0
    
    var rhythmState: [RhythmStroke] {
        get {
            return rhythm;
        }
    }
    
    public var rhythmStrokes: [Stroke] {
        get {
            return self.rhythm.map{$0.stroke}
        }
    }
    
    init(_ rhythm: [Stroke] = [Stroke.DOWN, Stroke.DOWN, Stroke.UP]){
        self.build(rhythm)
    }
    
    func build(_ rhythm: [Stroke]) {
        self.rhythm.removeAll()
        for stroke in rhythm {
            self.rhythm.append(RhythmStroke(stroke))
        }
    }
    
    func resetState() {
        for stroke in self.rhythm {
            stroke.clear()
        }
        
        self.currentPosition = 0
    }
    
    private func updateStrokeState(position: Int, valid: Bool){
        self.rhythm[position].updateState(valid: valid)
    }
    
    func validateStroke(_ stroke: Stroke) -> Bool {
        if(self.currentPosition >= self.rhythm.count){
            self.resetState()
        }
        
        let isValid = self.rhythm[self.currentPosition].stroke == stroke
        
        self.updateStrokeState(position: self.currentPosition, valid: isValid)
        self.currentPosition += 1
        
        return isValid
    }
}
