//  Helper.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2021 Marek Přitasil. All rights reserved.
//


import Foundation
import WatchKit

class Helper {
    private static var strokeMap: [Stroke: String] = [
        Stroke.UP: "\u{2191}",
        Stroke.DOWN: "\u{2193}"
    ]
    
    static func stringifyRhythm(_ rhythm: [Stroke]) -> String {
        return rhythm.reduce("",{result, stroke in
            return result + self.resolveStrokeChar(stroke)
        })
    }
    
    static func stringifyAndColorRhythmState(_ rhythmState: [RhythmStroke]) -> NSAttributedString {
        let result = NSMutableAttributedString()
        for stroke in rhythmState {
            let color = (stroke.state == StrokeState.PENDING) ?
                UIColor.lightGray : (stroke.state == StrokeState.VALID) ?
                UIColor.green : UIColor.red
            
            result.append(NSMutableAttributedString(string: self.resolveStrokeChar(stroke.stroke), attributes: [
                NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 23)
            ]))
            
        }
        return result
    }
    
    private static func resolveStrokeChar(_ stroke: Stroke) -> String {
        return strokeMap[stroke] ?? ""
    }
}
