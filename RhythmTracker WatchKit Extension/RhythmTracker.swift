//  RhythmTracker.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2021 Marek Přitasil. All rights reserved.
//


import Foundation
import os.log
import WatchKit
import HealthKit

protocol RhythmTrackerDelegate: class {
    func trackerSessionStarted()
    func trackerSessionEnded()
    func strokeDetected(valid: Bool, rhythmState: [RhythmStroke])
}


class RhythmTracker: NSObject, HKWorkoutSessionDelegate, AudioTrackerDelegate, MotionTrackerDelegate {
    private let interval: Double = 1/120
    private let audioTracker = AudioTracker()
    private let motionTracker = MotionTracker()
    private let healthStore = HKHealthStore()
    
    weak var delegate: RhythmTrackerDelegate?
    private var session: HKWorkoutSession?
    private var startTime: DispatchTime?
    private var numOfStrokes = 0
    private var rhythm: Rhythm = Rhythm()
    
    var rhythmStrokes: [Stroke] {
        get {
            return self.rhythm.rhythmStrokes
        }
    }
    
    var rhythmState: [RhythmStroke] {
        get {
            return self.rhythm.rhythmState
        }
    }
    
    override init() {
        super.init()
        self.audioTracker.delegate = self
        self.motionTracker.delegate = self
    }
    
    func startTracking() {
        if(session == nil ) {
            self.session = startSession()
            self.audioTracker.startRecording(interval: interval)
            self.motionTracker.startTracking(interval: interval)
            self.startTime = DispatchTime.now()
            self.numOfStrokes = 0
            self.rhythm.resetState()
            self.delegate?.trackerSessionStarted()
            os_log("Session started")
        }
    }
    
    func stopTracking() {
        self.audioTracker.stopRecording()
        self.motionTracker.stopTracking()
        self.stopSession()
        self.delegate?.trackerSessionEnded()
        os_log("Session stopped")
    }
    
    func soundDetected() {
        self.motionTracker.detectMotion(interval: interval)
    }
    
    func strokeDetected(stroke: Stroke, motionRate: Double){
        if(stroke == Stroke.UP){
            os_log("Upstroke: %.4f, time: %.5f:", motionRate, self.getElapsedTime())
        } else{
            os_log("Downstroke: %.4f, time: %.5f:", motionRate, self.getElapsedTime())
        }
        
        self.delegate?.strokeDetected(valid: self.rhythm.validateStroke(stroke), rhythmState: self.rhythm.rhythmState)
        self.numOfStrokes += 1
    }
    
    
    func updateRhythm(_ rhythm: [Stroke]){
        self.rhythm.build(rhythm)
    }
    
    
    private func startSession() -> HKWorkoutSession? {
        let configuration = HKWorkoutConfiguration()
        configuration.activityType = .other
        configuration.locationType = .indoor
        
        let session : HKWorkoutSession
        do {
            session = try HKWorkoutSession(healthStore: healthStore, configuration: configuration)
        } catch _ {
            os_log("session fail")
            WKInterfaceDevice.current().play(.failure)
            return nil
        }
        
        session.delegate = self
        session.startActivity(with: Date())
        
        return session
    }
    
    private func stopSession() {
        self.session?.end()
        self.session = nil
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        os_log("%s", error.localizedDescription)
    }
    
    func getElapsedTime() -> Double {
        return (Double(DispatchTime.now().uptimeNanoseconds - self.startTime!.uptimeNanoseconds)/1000000)
    }
}
