//  RhythmController.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2021 Marek Přitasil. All rights reserved.
//


import Foundation
import os
import WatchKit

class RhythmController: WKInterfaceController, RhythmTrackerDelegate {
    @IBOutlet weak var lbl: WKInterfaceLabel!
    
    var rhythmTracker: RhythmTracker?
    var running: Bool = false
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        self.rhythmTracker = context as? RhythmTracker
        self.rhythmTracker!.delegate = self
    }
    
    private func startSession(){
        if(!running) {
            self.running = true
            WKInterfaceDevice.current().play(.click)
            updateLabel(self.rhythmTracker!.rhythmState)
            rhythmTracker!.startTracking()
        }
    }
    
    private func stopSession(){
        self.running = false
        WKInterfaceDevice.current().play(.click)
        rhythmTracker!.stopTracking()
    }
    
    override func willActivate() {
        super.willActivate()
        self.startSession()
    }
    
    override func willDisappear() {
        self.stopSession()
        super.willDisappear()
    }
    
    func strokeDetected(valid: Bool, rhythmState: [RhythmStroke]) {
        os_log("is valid %d", valid)
        if(!valid){
            WKInterfaceDevice.current().play(.failure)
        }
        
        updateLabel(rhythmState)
    }
    
    private func updateLabel(_ rhythmState: [RhythmStroke]) {
        self.lbl.setAttributedText(Helper.stringifyAndColorRhythmState(rhythmState))
    }
    
    func trackerSessionStarted() {
        
    }
    
    func trackerSessionEnded() {
        dismiss()
    }
    
}
