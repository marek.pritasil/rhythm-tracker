//  MotionTracker.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2020 Marek Přitasil. All rights reserved.

import Foundation
import CoreMotion
import os.log
import WatchKit

protocol MotionTrackerDelegate: class {
    func strokeDetected(stroke: Stroke, motionRate: Double)
}

class MotionTracker {
    private let motionManager = CMMotionManager ()
    private let queue = OperationQueue()
    
    weak var delegate: MotionTrackerDelegate?
    private var detecting: Bool = false
    private var dataBuffer: Buffer!
    
    init() {
        queue.maxConcurrentOperationCount = 1
        queue.name = "MotionManagerQueue"
    }
    
    
    func startTracking(interval: TimeInterval) {
        self.motionManager.deviceMotionUpdateInterval = interval
        self.motionManager.startDeviceMotionUpdates()
        
        self.motionManager.startDeviceMotionUpdates(to: self.queue) { (motionData: CMDeviceMotion?, error: Error?) in
            if error != nil {
                print("Error: \n", error.debugDescription)
            }
            
            if motionData != nil {
                self.processDeviceMotion(motionData!, interval: interval)
            }
        }
        
    }
    
    func stopTracking() {
        self.motionManager.stopDeviceMotionUpdates()
        self.detecting = false
    }
    
    func detectMotion(interval: TimeInterval) {
        if(!self.detecting) {
            self.dataBuffer = Buffer(size: Int((1/interval)/6))
            self.detecting = true
        }else{
            // print("whoops")
        }
    }
    
    private func processDeviceMotion(_ motionData: CMDeviceMotion, interval: TimeInterval) {
        let product = motionData.rotationRate.z*abs(motionData.userAcceleration.y)*abs(2*motionData.rotationRate.x)*abs(motionData.gravity.y)
        if(self.detecting){
            self.dataBuffer.add(product)
            if(!(self.dataBuffer.isFull())) {
                return
            }
            
            let sum = self.dataBuffer.sum()
            if(0.15.isLess(than: abs(sum))){
                if(0.0.isLess(than: sum)){
                    self.delegate?.strokeDetected(stroke: Stroke.UP, motionRate: sum)
                }else {
                    self.delegate?.strokeDetected(stroke: Stroke.DOWN, motionRate: sum)
                }
            }            
            self.detecting = false
        }
    }
    
}
