//  InterfaceController.swift
//  RhythmTracker WatchKit Extension
//
//  Created by Marek Přitasil.
//  Copyright © 2020 Marek Přitasil. All rights reserved.
//

import WatchKit
import Foundation
import os.log

class MainController: WKInterfaceController {
    @IBOutlet weak var startBtn: WKInterfaceButton!
    @IBOutlet weak var rhythmBtn: WKInterfaceButton!
    
    private let rhythmTracker = RhythmTracker()
    
    override init() {
        super.init()
    }
    
    @IBAction func rhythmBtnPress() {
        self.pushController(withName: "RhythmSelectionController", context: self.rhythmTracker)
    }
    
    @IBAction func startBtnPress() {
        self.setStartBtnState(text: "Starting", enabled: false)
        self.presentController(withName: "RhythmController", context: self.rhythmTracker)
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        self.setRhythmBtnState(text: Helper.stringifyRhythm(self.rhythmTracker.rhythmStrokes), enabled: true)
        self.setStartBtnState(text: "Start", enabled: true)
    }
    

    private func setRhythmBtnState(text: String, enabled: Bool){
        self.rhythmBtn.setTitle(text)
        self.rhythmBtn.setEnabled(enabled)
    }
    
    private func setStartBtnState(text: String, enabled: Bool){
        self.startBtn.setTitle(text)
        self.startBtn.setEnabled(enabled)
    }
}
